# translation dutch <-> frisian #

### What is this repository for? ###

* Software for translating dutch to frisian and visa versa based on parallel corpora.  
  
![schema](oersetter.png)

### How do I get set up? ###

```
ed@faubu1:~$ git clone https://github.com/proycon/LaMachine.git
ed@faubu1:~$ cd LaMachine/
ed@faubu1:~/LaMachine$ ./bootstrap.sh --install oersetter
Where do you want to install LaMachine?
  1) in a local user environment
       installs as much as possible in a separate directory
       for a particular user; can exists alongside existing
       installations. May also be used (limited) by multiple
       users/groups if file permissions allow it. Can work without
       root but only if all global dependencies are already satisfied.
       (uses virtualenv)
  2) in a Virtual Machine
       complete separation from the host OS
       (uses Vagrant and VirtualBox)
  3) in a Docker container
       (uses Docker and Ansible)
  4) Globally on this machine
       dedicates the entire machine to LaMachine and
       modifies the existing system and may
       interact with existing packages. Usually requires root.
  5) On a remote server
       modifies an existing remote system! Usually requires root.
       (uses ansible)
  6) in an LXC/LXD container
       Provides a more persistent and VM-like container experience than Docker
       (uses LXD, LXC and Ansible)
  7) in a Singularity container (EXPERIMENTAL!)
       (uses Singularity and Ansible)
Your choice? [12345] 3
Do you want to build a new personalised LaMachine image or use and download a prebuilt one?
  1) Build a new image
       Offers most flexibility and ensures you are on the latest versions.
       Allows you to choose even for development versions or custom versions.
       Allows you to choose what software to include from scratch.
       Best integration with your custom data.
  2) Download a prebuilt one
       Comes with a fixed selection of software, allows you to update with extra software later.
       Fast & easy but less flexible
Your choice? [12] 1
Container diskspace
  A standard docker container is limited in size (usually 10GB). If you plan to include certain very large optional software
  collections that LaMachine offers (such as kaldi, valkuil) then this is not sufficient and
  you need to increase the base size of your containers (depending on the storage driver you use for docker).
  Consult the docker documentation at https://docs.docker.com/storage/storagedriver/ and do so now if you need this.
  You don't need this for the default selection of software.
Press ENTER when ready to continue
LaMachine comes in several versions:
 1) a stable version; you get the latest releases deemed stable (recommended)
 2) a development version; you get the very latest development versions for testing, this may not always work as expected!
 3) custom version; you decide explicitly what exact versions you want (for reproducibility);
    this expects you to provide a LaMachine version file (customversions.yml) with exact version numbers.
Which version do you want to install? [123] 1

The installation relies on certain software to be available on your (host)
system. It will be automatically obtained from your distribution's package manager
or another official source whenever possible. You need to have sudo permission for this though...
Answering 'no' to this question may make installation on your system impossible!

Do you have administrative access (root/sudo) on the current system? [yn] y
Your LaMachine installation is identified by a name (local env name, VM name) etc..
(This does not need to match the hostname or domain name, which is a separate configuration setting)
Enter a name for your LaMachine installation (no spaces!): oersetter
The hostname or fully qualified domain name (FDQN) determines how your LaMachine installation can be referenced on a network.
Please enter the hostname (or FQDN) of the LaMachine system (just press ENTER if you want to use oersetter here): oersetter.frisian.eu
```
